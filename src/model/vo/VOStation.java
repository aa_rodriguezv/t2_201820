package model.vo;

public class VOStation implements Comparable<VOStation>{

	
	private int id;
	
	private String name;
	
	private String city;
	
	private double latitude;
	
	private double longitude;
	
	private int capacity;
	
	private String date;
	
	public VOStation(int iD, String nName, String nCity, double nLatitude, double nLongitude, int nCapacity, String nDate) {
		// TODO Auto-generated constructor

		id = iD;
		name = nName;
		latitude = nLatitude;
		longitude = nLongitude;
		capacity = nCapacity;
		date = nDate;
	}
	
	
	
	public int getId() {
		return id;
	}





	public String getName() {
		return name;
	}





	public String getCity() {
		return city;
	}





	public double getLatitude() {
		return latitude;
	}





	public double getLongitude() {
		return longitude;
	}





	public int getCapacity() {
		return capacity;
	}





	public String getDate() {
		return date;
	}





	@Override
	public int compareTo(VOStation arg0) {
		// TODO Auto-generated method stub
		
		int equalityValue = 1000;
		
		if(this.getId() == arg0.getId())
		{
			equalityValue = 0;
		}
		else if(this.getId() > arg0.getId())
		{
			equalityValue = 1;
		}
		else 
		{
			equalityValue = -1;
		}
		
		return equalityValue;
	}

	
}
