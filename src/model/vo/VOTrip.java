package model.vo;

/**
 * Representation of a Trip object
 */
public class VOTrip implements Comparable<VOTrip>{

	/**
	 * 
	 */
	private int id;
	
	/**
	 * 
	 */
	private double tripSeconds;
	
	/**
	 * 
	 */
	private String fromStation;
	
	/**
	 * 
	 */
	private String toStation;
	

	private VOByke byke;
	
	private String gender;
	
	/**
	 * 
	 */
	public VOTrip(int iD, double nSeconds, String fStation, String tStation, VOByke nByke, String nGender) {
		
		id = iD;
		tripSeconds = nSeconds;
		fromStation = fStation;
		toStation = tStation;
		

		byke = nByke;
		gender = nGender;
		
	}
	
	/**
	 * @return id - Trip_id
	 */
	public int id() {
		// TODO Auto-generated method stub
		return id;
	}	
	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public double getTripSeconds() {
		// TODO Auto-generated method stub
		return tripSeconds;
	}

	/**
	 * @return station_name - Origin Station Name .
	 */
	public String getFromStation() {
		// TODO Auto-generated method stub
		return fromStation;
	}
	
	/**
	 * @return station_name - Destination Station Name .
	 */
	public String getToStation() {
		// TODO Auto-generated method stub
		return toStation;
	}

	
	
	/**
	 * 
	 * @return
	 */
	public VOByke getByke() {
		return byke;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getGender() {
		return gender;
	}

	@Override
	public int compareTo(VOTrip arg0) {
		// TODO Auto-generated method stub
		int equalityValue = 1000;
		
		if(this.id() == arg0.id())
		{
			equalityValue = 0;
		}
		else if(this.id() > arg0.id())
		{
			equalityValue = 1;
		}
		else 
		{
			equalityValue = -1;
		}
		
		return equalityValue;
	}
}
