package model.data_structures;

public class Node<T> {

	/**
	 * 
	 */
	private Node next;
	
	/**
	 * 
	 */
	private Node previous;
	
	/**
	 * 
	 */
	private T elemento;
	
	/**
	 * 
	 * @param elementoX
	 */
	public Node(T elementoX)
	{
		 elemento = elementoX;
		 next = null;
		 previous = null;
	}
	
	/**
	 * 
	 * @return
	 */
	public T darElemento()
	{
		return elemento;
	}
	
	/**
	 * 
	 * @return
	 */
	public Node darSiguiente()
	{
		return next;
	}
	
	/**
	 * 
	 * @return
	 */
	public Node darAnterior()
	{
		return previous;
	}
	
	/**
	 * 
	 */
	public void cambiarSiguiente(Node pNext)
	{
		this.next = pNext;
	}
	
	/**
	 * 
	 */
	public void cambiarAnterior(Node pPrevious)
	{
		this.previous = pPrevious;
	}
	
}
