package model.data_structures;
import java.util.LinkedList;

/**
 * Abstract Data Type for a doubly-linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IDoublyLinkedList<T extends Comparable<T>> extends Iterable<T> {

	Integer getSize();
	
	void add(T elemento);
	
	void addAtEnd(T elemento);
	
	void addAtK(T elemento, int k) throws Exception;
	
	T getElement(int posicion);
	
	T getCurrentElement();
	
	void delete(T elemento);
	
	void deleteAtK(int k) throws Exception;
	
	T next(T elemento);
	
	T previous(T elemento);

}
