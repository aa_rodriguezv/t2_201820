package model.data_structures;

import java.util.Iterator;

public class DoublyLinkedList< T extends Comparable<T>> implements IDoublyLinkedList<T> {

	/**
	 * 
	 */
	private Node primerNodo;
	
	
	
	public DoublyLinkedList() {
		// TODO Auto-generated constructor stub
		primerNodo = null;																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																												
	}
	
	@Override
	public Iterator<T> iterator() {
		
		return this.iterator();
	}
	
	@Override
	public Integer getSize() {
		
		Integer tamano = new Integer(0);
		
		if(primerNodo != null)
		{
			
			Node temp = primerNodo;
			
			while(temp != null )
			{
				tamano++;
				temp = temp.darSiguiente();
			}
		}
		
		return tamano;
	}

	
	@Override
	public void add(T elemento) {
		// TODO Auto-generated method stub
		Node nuevoNodo = new Node(elemento);
		
		if(primerNodo!= null)
		{
			Node temp = primerNodo;
			primerNodo = nuevoNodo;
			primerNodo.cambiarSiguiente(temp);
			temp.cambiarAnterior(primerNodo);
		}
		else
		{
			primerNodo = nuevoNodo;
		}
	}

	
	@Override
	public void addAtEnd(T elemento) {
	
		Node nuevoNodo = new Node(elemento);
		
		if(primerNodo != null)
		{
			Node ultimo = primerNodo;
			
			while(ultimo.darSiguiente() != null)
			{
				ultimo = ultimo.darSiguiente();
			}
			
			ultimo.cambiarSiguiente(nuevoNodo);
			nuevoNodo.cambiarAnterior(ultimo);
			
		}
		else
		{
			primerNodo = nuevoNodo;
		}

	}

	
	@Override
	public void addAtK(T elemento, int k) throws Exception {

		int posicion = 0;
		
		Node nuevoNodo = null;
		
		boolean agregado = false;
		
		if(primerNodo != null)
		{
			nuevoNodo = new Node(elemento);
			
			Node temp = primerNodo;
			
			
			while(temp != null && !agregado)
			{
				if(posicion == k)
				{
					
					nuevoNodo.cambiarSiguiente(temp);
					nuevoNodo.cambiarAnterior(temp.darAnterior());
					
					temp.darAnterior().cambiarSiguiente(nuevoNodo);
					temp.cambiarAnterior(nuevoNodo);
					
					agregado = true;
				}
				
				temp = temp.darSiguiente();
				posicion++;
			}
			
		}

		if(!agregado && k>(posicion-1))
		{
			throw new Exception("No existe tal posicion en la lista, la ultima posicion que hay es la numero: " + posicion);
		}
	}

	
	@Override
	public T getElement(int posicion) {
		// TODO Auto-generated method stub
		
		boolean encontrado = false;
		
		Node temp = primerNodo;
		Node elNodo = null;
		int contador = 0;
		
		while(temp != null && !encontrado)
		{
			if(contador == posicion)
			{
				encontrado = true;
				elNodo = temp;
			}
			temp = temp.darSiguiente();
		}
		
		return (T) elNodo.darElemento();
	}

	
	@Override
	public T getCurrentElement() {
	
		Node temp = primerNodo;
		
		while(temp != null)
		{
			temp = temp.darSiguiente();
		}
		
		return (T) temp.darElemento();
	}

	
	@Override
	public void delete(T elemento) {
		// TODO Auto-generated method stub
		Node temp = primerNodo;
		boolean encontrado = false;
		
		while(temp != null && !encontrado)
		{
			Node prev = temp.darAnterior();
			Node next = temp.darSiguiente();
			
			if(elemento.compareTo((T)temp.darElemento()) == 0)
			{
				encontrado = true;
				next.cambiarAnterior(prev);
				prev.cambiarSiguiente(next);
			}
			else
			{
				temp = temp.darSiguiente();
			}
		}
	}

	
	@Override
	public void deleteAtK(int k) throws Exception{
		
		if(primerNodo != null)
		{
			int posicion = 0;
			Node temp = primerNodo;
			boolean encontrado = false;
			
			while(temp != null && !encontrado)
			{
				if(posicion == k)
				{
					Node siguiente = temp.darSiguiente();
					Node anterior = temp.darAnterior();
					
					siguiente.cambiarAnterior(anterior);
					anterior.cambiarSiguiente(siguiente);
					
					encontrado = true;
				}
				
				temp = temp.darSiguiente();
				posicion++;
			}
			
			if(!encontrado && k>posicion)
			{
				throw new Exception("No existe tal posicion en la lista, la ultima posicion que hay es la numero: " + posicion);
			}
		}
		else
		{
			throw new Exception("No hay ningun elemento en la lista");
		}
	}


	@Override
	public T next(T elemento) {
		Node elNodo = null;
		Node temp = primerNodo;
		boolean encontrado = false;
		
		while(temp != null && !encontrado)
		{
			if(elemento.compareTo((T) temp.darElemento()) == 0)
			{
				elNodo = temp.darSiguiente();
				encontrado = true;
			}
			
			temp = temp.darSiguiente();
		}
		
		if(elNodo != null)
		{
			return (T) elNodo.darElemento();
		}
		
		return null;
	}
	
	@Override
	public T previous(T elemento) {
		Node elNodo = null;
		Node temp = primerNodo;
		boolean encontrado = false;
		
		while(temp != null && !encontrado)
		{
			if(elemento.compareTo((T) temp.darElemento()) == 0)
			{
				elNodo = temp.darAnterior();

				encontrado = true;
			}
			
			temp = temp.darSiguiente();
		}
		
		if(elNodo != null)
		{
			return (T) elNodo.darElemento();
		}
		
		return null;
	}

}
