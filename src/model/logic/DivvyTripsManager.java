package model.logic;

import java.io.FileNotFoundException;

import java.io.FileReader;
import java.io.IOException;

import com.opencsv.*;

import api.IDivvyTripsManager;
import model.vo.VOByke;
import model.vo.VOStation;
import model.vo.VOTrip;
import model.data_structures.DoublyLinkedList;

public class DivvyTripsManager implements IDivvyTripsManager {

	
	private DoublyLinkedList<VOTrip> trips;
	
	private DoublyLinkedList<VOStation> stations;
	
	
	public DivvyTripsManager() {
		// TODO Auto-generated constructor stub
		
		trips = new DoublyLinkedList<>();
		
		stations = new DoublyLinkedList<>();
	}
	
	
	public void loadStations (String stationsFile) {
		
		try {
			int contador = 1;
			CSVReader reader;
			reader = new CSVReader(new FileReader(stationsFile));
			while( reader.iterator().hasNext())
			{
				String[] datos = reader.readNext();
				
				int id = Integer.parseInt(datos[0]);
				String name = datos[1];
				String city = datos[2];
				double latitude = Double.parseDouble(datos[3]);
				double longitude = Double.parseDouble(datos[4]);
				int capacity = Integer.parseInt(datos[5]);
				String date = datos[6];
				VOStation theStation = new VOStation(id, name, city, latitude, longitude, capacity, date);
				
				stations.add(theStation);
				
				System.out.println(contador);
				contador++;
			}
				
			reader.close();
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}

	
	public void loadTrips (String tripsFile) {
		
		try {
			int contador = 1;
			CSVReader reader;
			reader = new CSVReader(new FileReader(tripsFile));
			while( reader.iterator().hasNext())
			{
				String[] datos = reader.readNext();
				
				int id = Integer.parseInt(datos[0]);
				
				int bikeID = Integer.parseInt(datos[3]);
				VOByke bike = new VOByke(bikeID);
				
				double duration = Double.parseDouble(datos[4]);
				String fromStation = datos[6];
				String toStation = datos[8];
				String genderType = datos[10];
				
				VOTrip trip = new VOTrip(id, duration, fromStation, toStation, bike, genderType);
				trips.add(trip);
				
				System.out.println(contador);
				contador++;
				
			}
			reader.close();	
			
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	
	@Override
	public DoublyLinkedList <VOTrip> getTripsOfGender (String gender) {
		
		DoublyLinkedList <VOTrip> tripsOfGender = new DoublyLinkedList <VOTrip>();
		
		for(int i = 0; i<trips.getSize(); i++)
		{
			if(trips.getElement(i).getGender().equals(gender))
			{
				tripsOfGender.add(trips.getElement(i));
			}
		}
		
		return tripsOfGender;
	}
	
	@Override
	public DoublyLinkedList <VOTrip> getTripsToStation (int stationID) {
		
		DoublyLinkedList <VOTrip> tripsToStation = new DoublyLinkedList <VOTrip>();
		
		for(int i = 0; i<trips.getSize(); i++)
		{
			if(trips.getElement(i).id() == stationID)
			{
				tripsToStation.add(trips.getElement(i));
			}
		}
		
		
		return tripsToStation;
	}	

}
